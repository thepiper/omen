# What's Omen?

*Currently under development* (daily usable, but an incredible pain in the ass to set up)

A machine-level installation of a server-capable [jupyterhub][https://jupyter.org/hub] without [Anaconda][https://www.anaconda.com/distribution/] or [Docker][https://www.docker.com/] requirements, with a number of shiny porcelain added to ease server fiddling and maintainence. Focus is on avoiding sudoing as root as much as possible.


## TODO list

* change to nginx over configurable-http-proxy
* set up a python script to initialize omen environment variables
* figure out installation with script or packaging upstream
* add logging
* enable OAuth
* use /srv/jupyterhub and /var/log
* throw it up online with actual LetsEncrypt certs


# Install

The install process is heavily based on the rootless process recommended by [jupyterhub][https://github.com/jupyterhub/jupyterhub/wiki/Using-sudo-to-run-JupyterHub-without-root-privileges]. There's a lot of fiddling with system-level components, so root access or judicious sudo is required. In snippets, `#` will be used to start sudo lines, while `$` will start non-sudo lines.

## Requirements

* Arch Linux - untested on other distros
* python 3.6+ with pip
* nodejs + npm -> `configurable-http-proxy` 
  * (for now, nginx once that has been implemented to working)
* TLS certificate - see certificates section below
* sudo - Arch does not come with sudo preinstalled

## Set up omen's user

This section is more or less verbatim from jupyterhub's rootless guide (see above).

Create a group to allow precise access control

```bash
# groupadd jupyterhub
```

Create the jupyterhub management user - call it whatever you want, jupyterhub officially uses `rhea`, here I use `omen` for obvious reasons.

```bash
# useradd -N omen
```

Add ourselves to the hub group
```bash
# usermod -aG jupyterhub omen
```

## Set up environment variables

First we need to inject the environment variables into the omen user, this is best done as omen - who does not have a user password - so we `su` in from root.

```bash
# su omen
omen $ touch ~/.bash_profile
```

Open up `~/.bash_profile` and add in some export lines:
```bash
export JUPYTERHUB_ROOT="/etc/jupyterhub"
export PATH="$JUPYTERHUB_ROOT/bin:$PATH"
```






# Starting the hub

First initialize omen by sourcing:

```
. omen
```

Omen adds a number of 

When dealing with hub scripts, use the `augur` alias to run things as omen.

```
augur omen-start
```

# Installation

WIP

requirements - nodejs+npm system install (or install with augur into omen's user directory)

# Issues

* SSL certificates with passwords will shit the bed 


## SSL Cert with password
The [official solution][1] is to set up env with the password as shell variable. The key should be hid in a local script file - be sure to setup gitignore if you need to share your configs somewhere.

[1]:https://github.com/jupyterhub/configurable-http-proxy/pull/175
